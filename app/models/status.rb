class Status < ApplicationRecord
  has_many :media
  belongs_to :user
  belongs_to :responded_to, class_name: :Status, optional: true
  has_many :responses, foreign_key: :responded_to_id, class_name: :Status
end
