class User < ApplicationRecord
  has_one_attached :avatar

  has_many :followees_records, foreign_key: :followee_id, class_name: 'Follow'
  has_many :followers, through: :followees_records, class_name: 'User'

  has_many :followers_records, foreign_key: :follower_id, class_name: 'Follow'
  has_many :followees, through: :followers_records, class_name: 'User'
end
