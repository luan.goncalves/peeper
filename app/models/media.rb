class Media < ApplicationRecord
  belongs_to :status
  has_one_attached :file

  enum media_type: [:image, :video]

  delegate :url, to: :file, allow_nil: true
end
