require 'rails_helper'

RSpec.describe Media, type: :model do
  it { should belong_to(:status) }
  it { should have_one_attached(:file) }
  it { should define_enum_for(:media_type).with_values([:image, :video]) }
  it { should delegate_method(:url).to(:file).allow_nil }
end
