require 'rails_helper'

RSpec.describe Status, type: :model do
  it { should belong_to(:user) }
  it { should have_many(:media) }
  it { should belong_to(:responded_to).class_name('Status').optional }
  it { should have_many(:responses).class_name('Status').with_foreign_key('responded_to_id') }
end
