require 'rails_helper'

RSpec.describe User, type: :model do
  it { should have_one_attached(:avatar) }
  it { should have_many(:followees_records).class_name('Follow').with_foreign_key('followee_id') }
  it { should have_many(:followers_records).class_name('Follow').with_foreign_key('follower_id') }
  it { should have_many(:followers).class_name('User').through('followees_records') }
  it { should have_many(:followees).class_name('User').through('followers_records') }
end
