class AddRespondedToIdToStatus < ActiveRecord::Migration[7.0]
  def change
    add_reference :statuses, :responded_to, null: true, foreign_key: false
  end
end
