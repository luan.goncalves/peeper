class CreateMedia < ActiveRecord::Migration[7.0]
  def change
    create_table :media do |t|
      t.integer :media_type, null: false, default: 0

      t.timestamps
    end
  end
end
