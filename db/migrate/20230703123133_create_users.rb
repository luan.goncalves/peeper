class CreateUsers < ActiveRecord::Migration[7.0]
  def change
    create_table :users do |t|
      t.string :handle, null: false
      t.string :display_name, null: false
      t.text :bio
      t.date :born_at, null: false

      t.timestamps
    end
  end
end
